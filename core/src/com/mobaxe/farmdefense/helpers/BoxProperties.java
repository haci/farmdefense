package com.mobaxe.farmdefense.helpers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class BoxProperties {

	private Body body;

	public Body getBody() {
		return body;
	}

	public BoxProperties(World world, float width, float height, Vector2 position) {

		// initialize body
		BodyDef bodyDef = new BodyDef();
		bodyDef.position.set(position);
		bodyDef.angle = 0;
		bodyDef.fixedRotation = true;
		body = world.createBody(bodyDef);
		body.setType(BodyType.StaticBody);

		// initialize shape
		FixtureDef fixtureDef = new FixtureDef();
		PolygonShape boxShape = new PolygonShape();
		boxShape.setAsBox(width / 2, height / 2);
		fixtureDef.shape = boxShape;
		fixtureDef.restitution = 0f; // positively bouncy!
		this.body.createFixture(fixtureDef);
		boxShape.dispose();
	}
}
