package com.mobaxe.farmdefense.helpers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class CustomSprite extends Sprite {

	public CustomSprite(Texture texture, float width, float height) {
		super(texture);
		setSize(width, height);
		setOriginCenter();
	}
}
