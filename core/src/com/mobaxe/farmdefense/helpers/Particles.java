package com.mobaxe.farmdefense.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Particles extends ParticleEffect {

	private String name;

	public Particles(String name, Vector2 position, float scale) {
		this.name = name;
		load(Gdx.files.internal("particle_effects/" + name), Gdx.files.internal("particle_effects"));
		getEmitters().first().setPosition(position.x, position.y);
		scaleEffect(scale);
		start();
	}

	public void update(SpriteBatch batch) {
		update(Gdx.graphics.getDeltaTime());
		batch.begin();
		draw(batch);
		batch.end();
		if (isComplete())
			reset();
	}

	public void dispose() {
		dispose();
	}

	public String getName() {
		return name;
	};
}
