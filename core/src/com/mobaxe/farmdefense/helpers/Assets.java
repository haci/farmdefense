package com.mobaxe.farmdefense.helpers;

import java.util.ArrayList;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class Assets {

	private static FreeTypeFontGenerator generator;
	private static FreeTypeFontParameter parameter;
	public static BitmapFont font;

	private static Sound sound;
	private static Music music;
	public static Texture splash, gameOver, homeBtn;
	public static Texture house, floor, gameBackground;
	public static Texture bulletImage, charfire, mainBG, shopBG;
	public static ArrayList<Texture> monsterHit = new ArrayList<Texture>();
	public static ArrayList<Texture> healthBarTexture = new ArrayList<Texture>();
	public static Texture rateMyApp, moreApps, rActive, mActive, exit, eActive, play, pActive, shop, sActive,
			back;

	public static Texture loadTexture(String filePath) {
		return new Texture(Gdx.files.internal(filePath));
	}

	public static Sound loadSound(String filePath) {
		sound = Gdx.audio.newSound(Gdx.files.internal(filePath));
		return sound;
	}

	public static Music loadMusic(String filePath) {
		music = Gdx.audio.newMusic(Gdx.files.internal(filePath));
		return music;
	}

	public static FileHandle loadJson(String filePath) {
		return Gdx.files.internal(filePath);
	}

	public static TextureAtlas loadAtlas(String filePath) {
		return new TextureAtlas(Gdx.files.internal(filePath));
	}

	public static void loadTexturesOnCreate() {

		// LOGO
		splash = loadTexture("images/logo.png");

		// UI
		rateMyApp = loadTexture("images/rateapp.png");
		rActive = loadTexture("images/rateappactive.png");
		moreApps = loadTexture("images/moreapp.png");
		mActive = loadTexture("images/moreappactive.png");
		play = loadTexture("images/play.png");
		pActive = loadTexture("images/pactive.png");
		shop = loadTexture("images/shop.png");
		sActive = loadTexture("images/sactive.png");
		exit = loadTexture("images/exit.png");
		eActive = loadTexture("images/eactive.png");
		back = loadTexture("images/back.png");

		// IMAGES
		gameBackground = loadTexture("images/background.jpg");
		house = loadTexture("models/house.png");
		floor = loadTexture("models/floor.png");
		bulletImage = loadTexture("images/bullet.png");
		charfire = loadTexture("images/charfire.png");
		mainBG = loadTexture("images/mainbg.jpg");
		shopBG = loadTexture("images/shopbg.jpg");
		gameOver = loadTexture("images/gameover.png");
		homeBtn = loadTexture("images/home.png");

		monsterHit.add(loadTexture("images/m1hit.png"));
		monsterHit.add(loadTexture("images/m2hit.png"));
		monsterHit.add(loadTexture("images/m3hit.png"));
		monsterHit.add(loadTexture("images/m4hit.png"));
		monsterHit.add(loadTexture("images/m5hit.png"));

		healthBarTexture.add(loadTexture("images/healthbar/h1.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h2.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h3.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h4.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h5.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h6.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h7.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h8.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h9.png"));
		healthBarTexture.add(loadTexture("images/healthbar/h10.png"));
		healthBarTexture.add(loadTexture("images/healthbar/death.png"));

		// FONT
		generateFont();

	}

	private static void generateFont() {
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/text.ttf"));
		parameter = new FreeTypeFontParameter();
		int density = 40;
		if (ApplicationType.Android == Gdx.app.getType()) {
			density = 8;
		}

		parameter.size = (int) (density * Gdx.graphics.getDensity());
		font = generator.generateFont(parameter);

		generator.dispose(); // don't forget to dispose to avoid memory leaks!

	}

	public static void dispose() {
		splash.dispose();
		rateMyApp.dispose();
		moreApps.dispose();
		gameBackground.dispose();
		house.dispose();
		floor.dispose();
		bulletImage.dispose();
		charfire.dispose();
		rateMyApp.dispose();
		rActive.dispose();
		moreApps.dispose();
		mActive.dispose();
		play.dispose();
		pActive.dispose();
		shop.dispose();
		sActive.dispose();
		exit.dispose();
		eActive.dispose();
		back.dispose();
	}
}