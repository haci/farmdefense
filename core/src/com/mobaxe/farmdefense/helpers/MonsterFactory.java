package com.mobaxe.farmdefense.helpers;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.mobaxe.farmdefense.gameobjects.Monster;
import com.mobaxe.farmdefense.utils.Utils;

public class MonsterFactory {

	private static Animation anim;
	private static AnimatedSprite animSprite;
	private static TextureAtlas atlas;
	private static FileHandle file;
	private static Vector2 pos;
	private static Vector2 spriteSize;
	private static float scale;

	public static Monster createMonster(World world, int no, float x, float y, float w, float h, float scl,
			boolean isBoss) {
		file = Assets.loadJson("models/monster" + no + ".json");// 1
		atlas = Assets.loadAtlas("anim/monster" + no + ".atlas");// 1
		pos = new Vector2(x, y);
		spriteSize = new Vector2(w, h);
		scale = scl;
		float fps = isBoss == true ? 0.65f : 0.25f;
		anim = new Animation(fps, atlas.getRegions());
		anim.setPlayMode(PlayMode.LOOP);
		animSprite = new AnimatedSprite(anim, true);

		return new Monster(file, world, animSprite, BodyType.KinematicBody, pos, spriteSize, scale, "monster"
				+ no, Utils.CATEGORY_MONSTER, no, isBoss);
	}
}
