package com.mobaxe.farmdefense;

import com.badlogic.gdx.Game;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.managers.EffectManager;
import com.mobaxe.farmdefense.managers.ScreenManager;
import com.mobaxe.farmdefense.screens.MyScreens;

public class FarmDefense extends Game {

	private static FarmPrefs prefs;

	public static ActionResolver actionResolver;

	public FarmDefense(ActionResolver actionResolver) {
		FarmDefense.actionResolver = actionResolver;
	}

	@Override
	public void create() {
		prefs = new FarmPrefs();

		Assets.loadTexturesOnCreate();
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.SPLASH_SCREEN);
	}

	public static FarmPrefs getPrefs() {
		return prefs;
	}

	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
		Assets.dispose();
		EffectManager.clearEffects();
	}
}
