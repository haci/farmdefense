package com.mobaxe.farmdefense.gameobjects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.helpers.CustomSprite;
import com.mobaxe.farmdefense.managers.GameManager;

public class Shooter extends EnvironmentObjects {

	private CustomSprite firedSprite;
	public boolean isAlive = true;

	public Shooter(FileHandle file, World world, Sprite sprite, BodyType bodyType, Vector2 pos,
			Vector2 spriteSize, float loaderScale, String name, short filter) {
		super(file, world, sprite, bodyType, pos, spriteSize, loaderScale, name, filter);
		this.sprite.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		firedSprite = new CustomSprite(Assets.charfire, 5, 5);
		firedSprite.setPosition(pos.x, pos.y);
	}

	@Override
	public void draw(SpriteBatch batch) {
		if (GameManager.weapon.isFired && GameManager.weapon.shouldReload == false) {
			firedSprite.draw(batch);
		} else {
			this.sprite.draw(batch);
		}
	}

}
