package com.mobaxe.farmdefense.gameobjects;

import java.util.ArrayList;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.helpers.Box2DFactory;
import com.mobaxe.farmdefense.helpers.CustomSprite;
import com.mobaxe.farmdefense.managers.GameManager;
import com.mobaxe.farmdefense.utils.Utils;

public class Weapon extends InputAdapter {

	public CustomSprite bulletSprite = new CustomSprite(Assets.bulletImage, 0.4f, 0.4f);;
	public Vector2 bulletPosition;
	public ArrayList<Body> bullets = new ArrayList<Body>();
	public int ammo = 0;

	private float bulletSpeed;
	private World world;
	private WeaponType weaponType;

	public boolean shouldReload = false;
	public boolean isFired = false;

	private Vector3 touchPos = new Vector3();

	public Weapon(World world, WeaponType weaponType) {
		this.world = world;
		this.weaponType = weaponType;
	}

	public void shoot(Vector2 direction) {
		if (GameManager.shooter.isAlive) {
			if (ammo > 10) {
				reload();
			} else {
				if (!shouldReload) {
					if (ammo != 0) {
						bullets.get(ammo - 1).setLinearVelocity(direction.scl(bulletSpeed));
					}
				}
			}
		}
	}

	public void reload() {
		shouldReload = true;
		ammo = 0;
		if (bullets.size() != 0) {
			for (Body body : bullets) {
				world.destroyBody(body);
			}
			bullets.clear();
		}
		Timer.schedule(new Task() {
			@Override
			public void run() {
				shouldReload = false;
			}
		}, 1f);

	}

	public void createBullet() {

		if (!shouldReload) {
			if (weaponType == WeaponType.GUN) {
				bulletSpeed = 75;
			}

			bulletPosition = new Vector2(-10.9f, 0.1f);

			EdgeShape shape = (EdgeShape) Box2DFactory.createEdgeShape(new Vector2(), new Vector2());
			FixtureDef fd = Box2DFactory.createFixture(shape, 0.1f, 0.1f, 0.1f, false);
			fd.filter.groupIndex = Utils.CATEGORY_BULLET;
			bullets.add(Box2DFactory.createBody(world, BodyType.DynamicBody, fd, bulletPosition, true));
			bullets.get(ammo).setUserData(bulletSprite);
			ammo += 1;
		}

	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (GameManager.shooter.isAlive) {
			GameManager.camera.unproject(touchPos.set(screenX, screenY, 0));
			createBullet();
			Vector2 direction = new Vector2(touchPos.x, touchPos.y);
			if (ammo != 0) {
				direction.sub(bullets.get(ammo - 1).getPosition()).nor();
			}
			shoot(direction);
			isFired = true;
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		isFired = false;
		return true;
	}
}
