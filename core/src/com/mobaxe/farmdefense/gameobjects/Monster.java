package com.mobaxe.farmdefense.gameobjects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.farmdefense.helpers.AnimatedSprite;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.managers.GameManager;

public class Monster extends EnvironmentObjects {

	private Animation anim;
	private AnimatedSprite animSprite;
	private TextureAtlas atlas;
	private int health;
	private int pos;
	private float linVelocity = -1.7f;
	public boolean hit = false;
	public boolean isReached = false;
	private Sprite hitSprite;
	public boolean isBoss = false;
	public boolean stopAttack = false;

	public Monster(FileHandle file, World world, Sprite sprite, BodyType bodyType, Vector2 pos,
			Vector2 spriteSize, float loaderScale, String name, short filter, int monsterNo, boolean isBoss) {
		super(file, world, sprite, bodyType, pos, spriteSize, loaderScale, name, filter);
		this.sprite.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

		this.isBoss = isBoss;
		setHealth(isBoss == true ? 20 : 4);

		atlas = Assets.loadAtlas("anim/m" + monsterNo + "attack.atlas");
		anim = new Animation(0.45f, atlas.getRegions());
		anim.setPlayMode(PlayMode.LOOP);
		animSprite = new AnimatedSprite(anim, true);
		animSprite.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		animSprite.setSize(spriteSize.x, spriteSize.y);

		hitSprite = new Sprite(Assets.monsterHit.get(monsterNo - 1));
		hitSprite.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		hitSprite.setSize(spriteSize.x, spriteSize.y);

	}

	@Override
	public void draw(SpriteBatch batch) {

		if (health != 0) {
			sprite.setPosition(body.getPosition().x, body.getPosition().y);
			if (body.getPosition().y == -4) {
				pos = 2;
			} else if (body.getPosition().y == -6) {
				pos = 2;
			} else if (body.getPosition().y == -8) {
				pos = -3;
			}

			if (body.getPosition().x < GameManager.house.body.getPosition().x + pos) {
				animSprite.setPosition(body.getPosition().x, body.getPosition().y);
				animSprite.draw(batch);
				body.setLinearVelocity(0, 0);
				isReached = true;
			} else {
				body.setLinearVelocity(linVelocity, 0);
				sprite.draw(batch);
			}
		}

		if (hit) {
			hitSprite.setPosition(body.getPosition().x, body.getPosition().y);
			hitSprite.draw(batch);
			Timer.schedule(new Task() {

				@Override
				public void run() {
					hit = false;
				}
			}, 0.12f);
		}

	}

	public int getHealth() {
		return health;
	};

	public void setHealth(int health) {
		this.health = health;
	};
}
