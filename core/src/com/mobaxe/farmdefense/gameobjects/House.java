package com.mobaxe.farmdefense.gameobjects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.helpers.CustomSprite;

public class House extends EnvironmentObjects {

	private CustomSprite healthBar;

	public House(FileHandle file, World world, Sprite sprite, BodyType bodyType, Vector2 pos,
			Vector2 spriteSize, float loaderScale, String name, short filter) {
		super(file, world, sprite, bodyType, pos, spriteSize, loaderScale, name, filter);
		healthBar = new CustomSprite(Assets.healthBarTexture.get(0), 7, 2);
		healthBar.setPosition(pos.x, pos.y + 6.5f);
	}

	@Override
	public void draw(SpriteBatch batch) {
		sprite.draw(batch);
		healthBar.draw(batch);
	}

	public void setHealthBarSprite(Texture healthBar) {
		this.healthBar.setTexture(healthBar);
	}

	public int getSpriteIndex() {
		int index = 0;
		for (int i = 0; i < Assets.healthBarTexture.size(); i++) {
			index = healthBar.getTexture().equals(Assets.healthBarTexture.get(i)) == true ? i : index;
		}
		return index;
	}
}
