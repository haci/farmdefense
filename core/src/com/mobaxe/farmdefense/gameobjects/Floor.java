package com.mobaxe.farmdefense.gameobjects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class Floor extends EnvironmentObjects {

	public Floor(FileHandle file, World world, Sprite sprite, BodyType bodyType, Vector2 pos,
			Vector2 spriteSize, float loaderScale, String name, short filter) {
		super(file, world, sprite, bodyType, pos, spriteSize, loaderScale, name, filter);
	}

	@Override
	public void draw(SpriteBatch batch) {
		sprite.draw(batch);
	}

}
