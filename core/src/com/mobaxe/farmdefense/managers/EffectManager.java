package com.mobaxe.farmdefense.managers;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.mobaxe.farmdefense.helpers.Particles;

public class EffectManager {

	public static ArrayList<Particles> effects = new ArrayList<Particles>();

	public static void loadAll() {
		effects.add(new Particles("bigFire", new Vector2(6, -2), 0.03f));
		effects.add(new Particles("firefly", new Vector2(-9, -7f), 0.06f));

	}

	public static void load(String name, Vector2 position, float scale) {
		effects.add(new Particles(name, position, scale));
	}

	public static void draw(SpriteBatch batch) {
		for (Particles effect : effects) {
			effect.update(batch);
		}

	}

	public static void clearLastEffect() {
		effects.remove(effects.size() - 1);
	}

	public static void clearEffects() {
		if (effects.size() != 0) {
			effects.clear();
		}
	}

}
