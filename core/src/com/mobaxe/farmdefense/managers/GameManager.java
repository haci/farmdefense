package com.mobaxe.farmdefense.managers;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.farmdefense.ActionResolver;
import com.mobaxe.farmdefense.FarmDefense;
import com.mobaxe.farmdefense.gameobjects.Floor;
import com.mobaxe.farmdefense.gameobjects.House;
import com.mobaxe.farmdefense.gameobjects.Monster;
import com.mobaxe.farmdefense.gameobjects.Shooter;
import com.mobaxe.farmdefense.gameobjects.Weapon;
import com.mobaxe.farmdefense.gameobjects.WeaponType;
import com.mobaxe.farmdefense.helpers.AnimatedSprite;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.helpers.CustomSprite;
import com.mobaxe.farmdefense.helpers.MonsterFactory;
import com.mobaxe.farmdefense.ui.HomeButton;
import com.mobaxe.farmdefense.utils.Utils;

public class GameManager {

	public static OrthographicCamera camera;
	public static SpriteBatch batch;
	public static Stage stage;

	private static World world;

	private static Floor floor;
	private static ArrayList<Monster> monsters = new ArrayList<Monster>();
	private static ArrayList<Body> monstersToRemove = new ArrayList<Body>();
	public static Shooter shooter;
	public static House house;
	public static Weapon weapon;

	private static Animation coinAnim;
	private static AnimatedSprite coinSprite;
	private static TextureAtlas coinAtlas;

	private static boolean generate = true;

	private static Random rnd = new Random();
	private static float deltaTime;
	private static boolean wait = false;
	private static int reachedMonster = 0;
	public static float damageTime = 2;
	private static Label coinsLabel;
	private static boolean isGameOver = false;
	private static CustomSprite gOver = new CustomSprite(Assets.gameOver, 15, 5);

	public static int adCounter = Utils.INTERSTITIAL_FREQ - 1;
	public static ActionResolver actionResolver;
	public static boolean adIsShowed = false;

	public static void init(World w, OrthographicCamera c, Stage s, SpriteBatch sb, ActionResolver ar) {
		world = w;
		camera = c;
		stage = s;
		batch = sb;
		actionResolver = ar;
	}

	public static void initWorld() {
		if (monsters.size() != 0) {
			monsters.clear();
			generate = true;
			reachedMonster = 0;
		}
		isGameOver = false;

		gOver.setPosition(-6.8f, 2.5f);

		// BACKGROUND
		Image background = new Image(Assets.gameBackground);
		background.setSize(Utils.virtualWidth, Utils.virtualHeight / 1.1f);
		background.setPosition(0, 63);
		stage.addActor(background);

		createHouse();

		createFloor();

		createShooter();

		createCoinLabelAndAnim();

		// WEAPON
		weapon = new Weapon(world, WeaponType.GUN);

	}

	private static void createCoinLabelAndAnim() {
		Table lblTable = new Table();
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = Assets.font;
		Color clr = new Color(1f, 0.8f, 0f, 1f);
		labelStyle.fontColor = clr;
		coinsLabel = new Label(String.valueOf(FarmDefense.getPrefs().getTotalBalance()), labelStyle);
		lblTable.add(coinsLabel).pad(0, 1350, 900, 0);

		HomeButton hButton = new HomeButton();
		Table btnTable = new Table();
		btnTable.setFillParent(true);
		btnTable.add(hButton).pad(0, 680, 330, 0);

		stage.addActor(btnTable);
		stage.addActor(lblTable);

		coinAtlas = Assets.loadAtlas("anim/coins.atlas");
		coinAnim = new Animation(0.06f, coinAtlas.getRegions());
		coinAnim.setPlayMode(PlayMode.LOOP);
		coinSprite = new AnimatedSprite(coinAnim, true);
		coinSprite.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		coinSprite.setSize(1.5f, 1.5f);
		coinSprite.setPosition(shooter.body.getPosition().x + 30, shooter.body.getPosition().y + 11.8f);
	}

	public static void updateStages(float delta) {
		stage.act(delta);
		stage.draw();
	}

	public static void updateSprites(float delta) {

		// PROJECTION MATRIX
		batch.setProjectionMatrix(camera.combined);

		// COIN SPRITE
		batch.begin();
		coinSprite.draw(batch);
		batch.end();

		// FLOOR SPRITE
		batch.begin();
		floor.draw(batch);
		batch.end();

		// HOUSE SPRITE
		batch.begin();
		house.draw(batch);
		batch.end();

		// BULLET SPRITE
		if (weapon.bullets.size() != 0) {
			for (int i = 0; i < weapon.bullets.size(); i++) {
				batch.begin();
				weapon.bulletSprite.draw(batch);
				weapon.bulletSprite.setPosition(weapon.bullets.get(i).getPosition().x, weapon.bullets.get(i)
						.getPosition().y);
				batch.end();
			}
		}

		// SHOOTER SPRITE
		batch.begin();
		shooter.draw(batch);
		batch.end();

		changeHealthBar();

		checkIfGameOver();

		// UPDATE CAMERA
		camera.update();

	}

	public static void showIntersititial() {

		// Ads every X refresh!
		if (adCounter == Utils.INTERSTITIAL_FREQ) {
			if (!adIsShowed) {
				actionResolver.showOrLoadInterstital();
				adIsShowed = true;
				adCounter--;
			}
		} else if (adCounter == 0) {
			adCounter = Utils.INTERSTITIAL_FREQ;
		} else {
			adCounter--;
		}

	}

	private static void checkIfGameOver() {
		if (isGameOver == false) {
			if (house.getSpriteIndex() == Assets.healthBarTexture.size() - 1) {
				isGameOver = true;
				shooter.isAlive = false;
				actionResolver.showOrLoadInterstital();
			}
		}
		if (isGameOver) {
			batch.begin();
			gOver.draw(batch);
			batch.end();
		}

	}

	private static void changeHealthBar() {
		for (int i = 0; i < monsters.size(); i++) {
			if (monsters.get(i).isReached) {
				if (wait == false) {
					reachedMonster++;
					wait = true;
					if (house.getSpriteIndex() != Assets.healthBarTexture.size() - 1) {
						house.setHealthBarSprite(Assets.healthBarTexture.get(house.getSpriteIndex() + 1));
					}
					damageTime = reachedMonster == 1 ? 2 : reachedMonster == 2 ? 1.2f
							: reachedMonster == 3 ? 0.8f : reachedMonster >= 4 ? 0.5f : 2;

					Timer.schedule(new Task() {
						@Override
						public void run() {
							wait = false;
						}
					}, damageTime);
				}
			}
		}
	}

	public static void updateMonsters() {

		if (isGameOver == false) {
			deltaTime += Gdx.graphics.getDeltaTime();

			// BOSS
			if (deltaTime > 30) {
				deltaTime = 0;
				monsters.add(MonsterFactory.createMonster(world, 5, 20, -4, 8, 7, 8, true));
			}

			if (generate) {
				generate = false;
				int no = rnd.nextInt(4) + 1;
				int posy = -6;
				int scl = 3;
				if (no == 4) {
					posy = 0;
					scl = 4;
				}
				monsters.add(MonsterFactory.createMonster(world, no, 21, posy, scl, scl, scl, false));
				
				Timer.schedule(new Task() {
					@Override
					public void run() {
						int no = rnd.nextInt(4) + 1;
						int posy = -6;
						int scl = 3;
						if (no == 4) {
							posy = 2;
							scl = 4;
						}
						monsters.add(MonsterFactory.createMonster(world, no, 25, posy, scl, scl, scl, false));
					}
				}, 1f);
				Timer.schedule(new Task() {
					@Override
					public void run() {
						int no = rnd.nextInt(4) + 1;
						int posy = -8;
						int scl = 3;
						if (no == 4) {
							posy = -1;
							scl = 4;
						}
						monsters.add(MonsterFactory.createMonster(world, no, 20, posy, scl, scl, scl, false));
					}
				}, 1.3f);

				Timer.schedule(new Task() {
					@Override
					public void run() {
						generate = true;
					}
				}, 8);

			}
		}

		if (monstersToRemove.size() != 0) {
			for (Body body : monstersToRemove) {
				world.destroyBody(body);
			}
			monstersToRemove.clear();
		}

		batch.begin();
		for (Monster monster : monsters) {
			monster.draw(batch);
		}
		batch.end();

	}

	public static void createContactListener() {

		world.setContactListener(new ContactListener() {

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void beginContact(Contact contact) {
				Body bb = contact.getFixtureB().getBody();
				Body ba = contact.getFixtureA().getBody();

				if (ba.isBullet()) {
					for (int i = 0; i < monsters.size(); i++) {
						if (bb.equals(monsters.get(i).body)) {
							monsters.get(i).setHealth(monsters.get(i).getHealth() - 1);
							monsters.get(i).hit = true;
							if (monsters.get(i).getHealth() == 0) {
								int scr = monsters.get(i).isBoss == true ? 20 : 10;
								FarmDefense.getPrefs().saveTotalBalance(
										FarmDefense.getPrefs().getTotalBalance() + scr);
								coinsLabel.setText(String.valueOf(FarmDefense.getPrefs().getTotalBalance()));
								monstersToRemove.add(bb);
								monsters.remove(i);
							}
						}
					}
				}

			}

		});
	}

	private static void createHouse() {
		FileHandle file;
		Sprite sprite;
		Vector2 pos;
		Vector2 spriteSize;
		float scale;

		// HOUSE
		file = Assets.loadJson("models/house.json");
		sprite = new Sprite(Assets.house);
		pos = new Vector2(-12.9f, 2);
		spriteSize = new Vector2(10, 19);
		scale = 11;

		house = new House(file, world, sprite, BodyType.StaticBody, pos, spriteSize, scale, "house",
				Utils.CATEGORY_SCENE);
	}

	private static void createFloor() {
		FileHandle file;
		Sprite sprite;
		Vector2 pos;
		Vector2 spriteSize;
		float scale;
		// FLOOR
		file = Assets.loadJson("models/floor.json");
		sprite = new Sprite(Assets.floor);
		pos = new Vector2(0, -10);
		spriteSize = new Vector2(38, 8);
		scale = 38;

		floor = new Floor(file, world, sprite, BodyType.StaticBody, pos, spriteSize, scale, "floor",
				Utils.CATEGORY_SCENE);
	}

	private static void createShooter() {
		Animation anim;
		AnimatedSprite animSprite;
		TextureAtlas atlas;
		FileHandle file;
		Vector2 pos;
		Vector2 spriteSize;
		float scale;
		// SHOOTER
		file = Assets.loadJson("models/shooter.json");
		atlas = Assets.loadAtlas("anim/shooter.atlas");
		pos = new Vector2(-15, -3);
		spriteSize = new Vector2(5, 5);
		scale = 3;
		anim = new Animation(0.50f, atlas.getRegions());
		anim.setPlayMode(PlayMode.LOOP);
		animSprite = new AnimatedSprite(anim, true);

		shooter = new Shooter(file, world, animSprite, BodyType.StaticBody, pos, spriteSize, scale,
				"shooter", Utils.CATEGORY_SHOOTER);
	}

}
