package com.mobaxe.farmdefense.managers;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.IntMap;
import com.mobaxe.farmdefense.FarmDefense;
import com.mobaxe.farmdefense.screens.MyScreens;

public class ScreenManager {

	private static ScreenManager instance;
	private FarmDefense game;
	private IntMap<Screen> screens;

	private ScreenManager() {
		screens = new IntMap<Screen>();
	}

	public static ScreenManager getInstance() {
		if (instance == null) {
			instance = new ScreenManager();
		}
		return instance;

	}

	public void initialize(FarmDefense game) {
		this.game = game;
	}

	public void show(MyScreens screen) {

		if (game == null) {
			return;
		}
		if (!screens.containsKey(screen.ordinal())) {
			screens.put(screen.ordinal(), screen.getScreenInstance());
		}
		game.setScreen(screens.get(screen.ordinal()));
	}

	public void dispose(MyScreens screen) {
		if (!screens.containsKey(screen.ordinal())) {
			return;
		}
		screens.remove(screen.ordinal()).dispose();
	}

	public void dispose() {
		for (Screen screen : screens.values()) {
			screen.dispose();
		}
		screens.clear();
		instance = null;
	}

}
