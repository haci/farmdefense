package com.mobaxe.farmdefense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class FarmPrefs {

	private String TOTAL_BALANCE = "totalbalance";

	private Preferences prefs = Gdx.app.getPreferences("farmPrefs");

	public void saveTotalBalance(int amount) {
		prefs.putInteger(TOTAL_BALANCE, amount);
		prefs.flush();
	}

	public int getTotalBalance() {
		return prefs.getInteger(TOTAL_BALANCE);
	}

	public void clearPrefs() {
		prefs.clear();
	}

}
