package com.mobaxe.farmdefense.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.managers.ScreenManager;
import com.mobaxe.farmdefense.screens.MyScreens;

public class PlayButton extends Button {

	private String buttonUp;
	private String buttonDown;
	private Skin skin;
	private ButtonStyle style;

	public PlayButton() {
		buttonUp = "ButtonUp";
		buttonDown = "ButtonDown";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, Assets.play);
		skin.add(buttonDown, Assets.pActive);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.down = skin.getDrawable(buttonDown);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				buttonFunction(event);
			}

			private void buttonFunction(InputEvent event) {

				event.getStage().addAction(
						Actions.sequence(Actions.fadeOut(.7f, Interpolation.pow5Out),
								Actions.run(new Runnable() {

									@Override
									public void run() {
										ScreenManager.getInstance().dispose(MyScreens.MAIN_MENU);
										ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
									}
								})));
			}

		});
	}
}
