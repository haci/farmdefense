package com.mobaxe.farmdefense.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Utils {

	public static final short CATEGORY_SCENE = -2;
	public static final short CATEGORY_SHOOTER = -2;
	public static final short CATEGORY_BULLET = -2;
	public static final short CATEGORY_MONSTER = 1;

	public static final int INTERSTITIAL_FREQ = 1;

	private static final float offset = 0.5f;
	private static final float width = 75;

	public static float realWidth = Gdx.graphics.getWidth();
	public static float realHeight = Gdx.graphics.getHeight();

	public static final float virtualWidth = 800;
	public static final float virtualHeight = 480;

	private static final float height = 75 * (virtualHeight / virtualWidth);

	public static final float widthMeters = width / 2 - offset;
	public static final float heightMeters = height / 2 - offset;

	public static final Vector2 GRAVITY = new Vector2(0, 0);

}
