package com.mobaxe.farmdefense.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.ui.ExitButton;
import com.mobaxe.farmdefense.ui.MoreButton;
import com.mobaxe.farmdefense.ui.PlayButton;
import com.mobaxe.farmdefense.ui.RateButton;
import com.mobaxe.farmdefense.ui.ShopButton;
import com.mobaxe.farmdefense.utils.Utils;

public class MainMenu implements Screen {

	private Stage stage;
	private Table bgTable;
	private Texture bgTexture;
	private Image bgImage;

	private PlayButton pb;
	private ShopButton sb;
	private ExitButton eb;
	private RateButton rb;
	private MoreButton mb;

	private ArrayList<Table> buttonTables = new ArrayList<Table>();

	public MainMenu() {
		bgTable = new Table();
		bgTable.setFillParent(true);
		for (int i = 0; i < 6; i++) {
			buttonTables.add(new Table());
			buttonTables.get(i).setFillParent(true);
		}
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {
		pb = new PlayButton();
		sb = new ShopButton();
		eb = new ExitButton();
		rb = new RateButton();
		mb = new MoreButton();

		buttonTables.get(0).add(pb).pad(0, 20, 330, 0);
		buttonTables.get(1).add(sb).pad(0, 190, 240, 0);
		buttonTables.get(2).add(eb).pad(0, 20, 160, 0);
		buttonTables.get(3).add(rb).pad(0, 190, 70, 0);
		buttonTables.get(4).add(mb).pad(30, 20, 0, 0);

		stage.addActor(bgTable);

		for (Table table : buttonTables) {
			stage.addActor(table);
		}
	}

	private void setBackgroundImage() {
		bgTexture = Assets.mainBG;
		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
