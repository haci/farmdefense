package com.mobaxe.farmdefense.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.managers.ScreenManager;
import com.mobaxe.farmdefense.utils.Utils;

public class SplashScreen implements Screen {

	private Stage stage;
	private Image logo;

	public SplashScreen() {
		logo = new Image(Assets.splash);
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.7f, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act();
		stage.draw();
	}

	Runnable onActionEnd = new Runnable() {
		@Override
		public void run() {
			ScreenManager.getInstance().dispose(MyScreens.SPLASH_SCREEN);
			ScreenManager.getInstance().show(MyScreens.MAIN_MENU);
		}
	};

	@Override
	public void show() {
		logo.setPosition(stage.getCamera().viewportWidth / 3.2f, stage.getCamera().viewportHeight);
		logo.addAction(Actions.sequence(Actions.moveTo(logo.getX(), logo.getY() - 400, 1.3f,
				Interpolation.bounceOut)));

		stage.addActor(logo);
		stage.addAction(Actions.sequence(Actions.delay(1.5f), Actions.fadeOut(1f, Interpolation.fade),
				Actions.run(onActionEnd)));

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}