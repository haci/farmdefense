package com.mobaxe.farmdefense.screens;

import com.badlogic.gdx.Screen;
import com.mobaxe.farmdefense.FarmDefense;

public enum MyScreens {

	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen(FarmDefense.actionResolver);
		}
	},
	MAIN_MENU {
		public Screen getScreenInstance() {
			return new MainMenu();
		}
	},
	SHOP_SCREEN {
		public Screen getScreenInstance() {
			return new ShopScreen();
		}
	},
	SPLASH_SCREEN {
		public Screen getScreenInstance() {
			return new SplashScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
