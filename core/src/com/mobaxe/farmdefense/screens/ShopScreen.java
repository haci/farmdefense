package com.mobaxe.farmdefense.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.ui.BackButton;
import com.mobaxe.farmdefense.utils.Utils;

public class ShopScreen implements Screen {

	private Stage stage;
	private Table bgTable;
	private Texture bgTexture;
	private Image bgImage;

	private Table btnTable;
	private BackButton bb;

	public ShopScreen() {
		bgTable = new Table();
		bgTable.setFillParent(true);
		btnTable = new Table();
		btnTable.setFillParent(true);
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {
		bb = new BackButton();
		btnTable.add(bb).pad(280, 650, 0, 0);

		stage.addActor(bgTable);
		stage.addActor(btnTable);
	}

	private void setBackgroundImage() {
		bgTexture = Assets.shopBG;
		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
