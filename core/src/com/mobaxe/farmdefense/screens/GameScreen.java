package com.mobaxe.farmdefense.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.farmdefense.ActionResolver;
import com.mobaxe.farmdefense.helpers.Assets;
import com.mobaxe.farmdefense.managers.EffectManager;
import com.mobaxe.farmdefense.managers.GameManager;
import com.mobaxe.farmdefense.utils.Utils;

public class GameScreen implements Screen {

	private OrthographicCamera camera;
	private World world;
	private Box2DDebugRenderer debugRenderer;
	private SpriteBatch batch;
	private Stage stage;

	private InputMultiplexer multiplexer;
	private float startTime;
	private ActionResolver actionResolver;

	public static Music gamePlaySound = Assets.loadMusic("sounds/gameplay.mp3");

	public GameScreen(ActionResolver actionResolver) {
		this.actionResolver = actionResolver;
	}

	@Override
	public void show() {
		startTime = 0;
		veryFirstInit();

		EffectManager.loadAll();
		GameManager.init(world, camera, stage, batch, actionResolver);
		GameManager.initWorld();
		GameManager.createContactListener();

		inputs();

		if (!gamePlaySound.isPlaying()) {
			gamePlaySound.setVolume(1.5f);
			gamePlaySound.setLooping(true);
			gamePlaySound.play();
		}

	}

	private void inputs() {
		multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(GameManager.weapon);
		Gdx.input.setInputProcessor(multiplexer);
	}

	private void veryFirstInit() {
		world = new World(Utils.GRAVITY, true);
		camera = new OrthographicCamera(Utils.widthMeters, Utils.heightMeters);
		debugRenderer = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		startTime += delta;
		debugRenderer.render(world, camera.combined);

		GameManager.updateStages(delta);
		GameManager.updateSprites(delta);
		EffectManager.draw(batch);
		if (startTime > 4) {
			GameManager.updateMonsters();
		}
		world.step(delta, 6, 2);

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		world.dispose();
		EffectManager.clearEffects();
	}

}
